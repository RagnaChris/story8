$(function(){
	var number = 0;
	let query = "Books"	;
	var pages = 1;

	function pagesCheck(len, number, pages){
		if (number < 10){
			$('#prev').hide();
		}
		else {
			$('#prev').show();	
		}
		if (number + 10 > len - 1){
			$('#next').hide();
		}
		else {
			$('#next').show();	
		}
		$('#page').text(pages);
	}

	function authorsCheck(img){
		if (img == undefined){
			return "-";
		}
		else {
			return img;
		}
	}

	function imgCheck(author){
		if (author == undefined){
			return "-";
		}
		else {
			return author;
		}
	}

	function printRange(len, number){
		if (number + 10 > len - 1){
			$('#range').text(`Page ${number + 1} - ${len}`);
		}
		else {
			$('#range').text(`Page ${number + 1} - ${number + 10}`);	
		}
	}

	$.ajax({
		url: "https://www.googleapis.com/books/v1/volumes?q=" + query,
		success: function(result){
			$('#found').text("Recommendation");
			pagesCheck(result.totalItems, number, pages);
			printRange(result.totalItems, number);
			for(i=0; i < result.items.length; i++) {
				$('tbody').append("<tr><td><img src='" + imgCheck(result.items[i].volumeInfo.imageLinks.thumbnail) + "'></td>" +
					"<td class='wrap long'>" + result.items[i].volumeInfo.title + "</td>" +
					"<td class='center wrap mid'>" + authorsCheck(result.items[i].volumeInfo.authors) + "</td>" +
					"</tr>");
			}
		}
	});
	$('#search').click(function(){
		$('tbody > tr').remove();
		number = 0;
		pages = 1;
		query = $('#kind').val()
		$.ajax({
			url: "https://www.googleapis.com/books/v1/volumes?q=" + query + "&startIndex=" + number,
			success: function(result){
				$('#found').text(query);
				pagesCheck(result.totalItems, number, pages);
				printRange(result.totalItems, number);
				for(i=0; i < result.items.length; i++) {
					$('tbody').append("<tr><td><img src='" + imgCheck(result.items[i].volumeInfo.imageLinks.thumbnail) + "'></td>" +
						"<td class='wrap long'>" + result.items[i].volumeInfo.title + "</td>" +
						"<td class='center wrap mid'>" + authorsCheck(result.items[i].volumeInfo.authors) + "</td>" +
						"</tr>");
				}
			}
		});
	});
	$('#next').click(function(){
		$('tbody > tr').remove();
		number += 10;
		pages += 1;
		$.ajax({
			url: "https://www.googleapis.com/books/v1/volumes?q=" + query + "&startIndex=" + number,
			success: function(result){
				$('#found').text(query);
				pagesCheck(result.totalItems, number, pages);
				printRange(result.totalItems, number);
				for(i=0; i < result.items.length; i++) {
					$('tbody').append("<tr><td><img src='" + imgCheck(result.items[i].volumeInfo.imageLinks.thumbnail) + "'></td>" +
						"<td class='wrap long'>" + result.items[i].volumeInfo.title + "</td>" +
						"<td class='center wrap mid'>" + authorsCheck(result.items[i].volumeInfo.authors) + "</td>" +
						"</tr>");
				}
			}
		});
	});
	$('#prev').click(function(){
		$('tbody > tr').remove();
		number -= 10;
		pages -= 1;
		$.ajax({
			url: "https://www.googleapis.com/books/v1/volumes?q=" + query + "&startIndex=" + number,
			success: function(result){
				$('#found').text(query);
				pagesCheck(result.totalItems, number, pages);
				printRange(result.totalItems, number);
				for(i=0; i < result.items.length; i++) {
					$('tbody').append("<tr><td><img src='" + imgCheck(result.items[i].volumeInfo.imageLinks.thumbnail) + "'></td>" +
						"<td class='wrap long'>" + result.items[i].volumeInfo.title + "</td>" +
						"<td class='center wrap mid'>" + authorsCheck(result.items[i].volumeInfo.authors) + "</td>" +
						"</tr>");
				}
			}
		});
	});
})