from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Story8Test(TestCase):
	def test_homepage_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_homepage_using_daftarbuku_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'daftarbuku.html')

	def test_homepage_using_homepage_func(self):
		found = resolve('/')
		self.assertEqual(found.func, homepage)